console.log("test")

/*
==============================================================================
Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.
	Log the 2 new Dog objects in the console or alert. */





  let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"],
	introduce1: () => {
		console.log(`Hi! I'm ${student1.name}, I'm ${student1.age} years old`);
		console.log(`I study the following courses ${student1.classes}`);
	}
}

student1.introduce1();

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"],
	introduce2: () => {
		console.log(`Hi! I'm ${student2.name}, I'm ${student2.age} years old`);
		console.log(`I study the following courses ${student2.classes}`);
	}
}

student2.introduce2();

function getCube(num) {

	console.log(Math.pow(num,3));
	// console.log("test");

}

function getCube(num){
    
	console.log(Math.pow(num,3));

}

let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

let numsSquared = numArr.map(function(num){

	return num ** 2;

})

console.log(numsSquared);


/* 

no.2 and 3

*/


class Dog {
	constructor(breed, name, dogAge) {
		this.breed = breed;
		this.name = name;
		this.dogAge = dogAge;
	}
};


let newDog = new Dog('rottweiler', 'covidoo', '7');
console.log(newDog);

class Dog1 {
	constructor(breed, name, dogAge, owner) {
		this.breed = breed;
		this.name = name;
		this.dogAge = dogAge;
		this.owner = owner
	}
};


let dog1 = new Dog1('german shepherd', 'astroboy', '7', 'James');
console.log(dog1);

class Dog2 {
	constructor(breed, name, dogAge, owner) {
		this.breed = breed;
		this.name = name;
		this.dogAge = dogAge;
		this.owner = owner;
	}
};


let dog2 = new Dog1('husky', 'bobix', '7', 'Karl');
console.log(dog2);
		